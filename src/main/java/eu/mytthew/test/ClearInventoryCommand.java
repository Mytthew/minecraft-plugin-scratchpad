package eu.mytthew.test;

import lombok.Value;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@Value
public class ClearInventoryCommand implements CommandExecutor {
	Main main;

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			return true;
		}
		Player player = (Player) sender;
		player.getInventory().clear();
		return true;
	}
}
