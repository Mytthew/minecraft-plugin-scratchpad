package eu.mytthew.test;

import lombok.Value;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.concurrent.ThreadLocalRandom;

@Value
public class ActionCommand implements CommandExecutor {
	Main main;

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length == 0) {
			sender.sendMessage(ChatColor.RED + "Error!");
			return true;
		}
		if (args.length == 1) {
			if (args[0].equals("text")) {
				sender.sendMessage(main.getRandomText());
				return true;
			}

			if (sender instanceof Player) {
				if (args[0].equals("time")) {
					Player player = (Player) sender;
					World world = player.getLocation().getWorld();
					long random = ThreadLocalRandom.current().nextLong(24000);
					if (world != null) {
						world.setTime(random);
					}
					return true;
				}

				if (args[0].equals("die")) {
					Player player = (Player) sender;
					player.setHealth(0);
					return true;
				}

				if (args[0].equals("block")) {
					Player player = (Player) sender;
					player.getLocation().add(1, 0, 0).getBlock().setType(Material.COBBLESTONE);
					player.getLocation().add(-1, 0, 0).getBlock().setType(Material.COBBLESTONE);
					player.getLocation().add(0, 0, 1).getBlock().setType(Material.COBBLESTONE);
					player.getLocation().add(0, 0, -1).getBlock().setType(Material.COBBLESTONE);

					player.getLocation().add(1, 0, 1).getBlock().setType(Material.COBBLESTONE);
					player.getLocation().add(1, 0, -1).getBlock().setType(Material.COBBLESTONE);
					player.getLocation().add(-1, 0, 1).getBlock().setType(Material.COBBLESTONE);
					player.getLocation().add(-1, 0, -1).getBlock().setType(Material.COBBLESTONE);
					// Second layer
					player.getLocation().add(1, 1, 0).getBlock().setType(Material.COBBLESTONE);
					player.getLocation().add(-1, 1, 0).getBlock().setType(Material.COBBLESTONE);
					player.getLocation().add(0, 1, 1).getBlock().setType(Material.COBBLESTONE);
					player.getLocation().add(0, 1, -1).getBlock().setType(Material.COBBLESTONE);

					player.getLocation().add(1, 1, 1).getBlock().setType(Material.COBBLESTONE);
					player.getLocation().add(1, 1, -1).getBlock().setType(Material.COBBLESTONE);
					player.getLocation().add(-1, 1, 1).getBlock().setType(Material.COBBLESTONE);
					player.getLocation().add(-1, 1, -1).getBlock().setType(Material.COBBLESTONE);
					// Above player
					player.getLocation().add(0, 2, 0).getBlock().setType(Material.COBBLESTONE);
					// Under player
					player.getLocation().add(0, -1, 0).getBlock().setType(Material.COBBLESTONE);
				}
			}
		}
		sender.sendMessage(command.toString() + String.join("", args));
		return true;
	}
}
