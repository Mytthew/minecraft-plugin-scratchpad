package eu.mytthew.test;

import lombok.NonNull;
import org.bukkit.Material;
import org.bukkit.command.CommandExecutor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public class Main extends JavaPlugin {
	private final Config config = new Config();
	private final Map<Player, LocalDateTime> times = new HashMap<>();

	public List<KitItem> getItemList() {
		return config.getItemList();
	}

	public int getDuration() {
		return config.getDuration();
	}

	public String getRandomText() {
		return config.getTextCollection().next();
	}

	public void setTime(final Player player, final LocalDateTime time) {
		times.put(player, time);
	}

	public LocalDateTime getTime(final Player player) {
		return times.get(player);
	}

	public void diamondChest() {
		final ItemStack diamondChest = new ItemStack(Material.CHEST);
		if (diamondChest.getItemMeta() != null) {
			final ItemMeta newItemMeta = diamondChest.getItemMeta();
			newItemMeta.setDisplayName("Diamond chest");
			diamondChest.setItemMeta(newItemMeta);
			final ShapedRecipe diamondChestRecipe = new ShapedRecipe(diamondChest)
					.shape("LLL", "OSO", "LLL")
					.setIngredient('L', Material.SPRUCE_LOG)
					.setIngredient('O', Material.OBSIDIAN)
					.setIngredient('S', Material.SLIME_BALL);
			getServer().addRecipe(diamondChestRecipe);
		}
	}

	public String getMessage(final String title) {
		return config.getMessagesMap().get(title);
	}

	private KitItem createKitItem(@NonNull final String material, final int amount) {
		return new KitItem(Material.matchMaterial(material), amount);
	}

	private void loadConfig() {
		final Optional<ConfigurationSection> configurationSection = Optional.ofNullable(getConfig().getConfigurationSection("kit"));

		configurationSection.map(cs -> cs.getInt("duration"))
				.ifPresent(config::setDuration);

		configurationSection.map(cs -> cs.getList("items"))
				.filter(item -> item instanceof Map)
				.map(item -> getConfig().createSection("x", (Map<?, ?>) item))
				.map(item -> createKitItem(item.getString("material"), item.getInt("amount")))
				.ifPresent(config.getItemList()::add);
		
		Objects.requireNonNull(getConfig()
				.getList("texts"))
				.stream()
				.filter(text -> text instanceof Map)
				.map(text -> getConfig().createSection("x", (Map<?, ?>) text))
				.forEach(text -> config.getTextCollection().add(text.getDouble("value"), text.getString("text")));

		Optional.ofNullable(getConfig().getConfigurationSection("messages"))
				.ifPresent(messages -> messages.getKeys(false)
						.forEach(key -> config.getMessagesMap().put(key, messages.getString(key))));
	}

	@Override
	public void onEnable() {
		final Map<String, CommandExecutor> commands = new HashMap<>();
		commands.put("action", new ActionCommand(this));
		commands.put("kit", new KitCommand(this));
		commands.put("clearinventory", new ClearInventoryCommand(this));
		commands.forEach((key, value) -> Objects.requireNonNull(getCommand(key)).setExecutor(value));
		saveDefaultConfig();
		loadConfig();
		getServer().getPluginManager().registerEvents(new ThrowListener(this), this);
		diamondChest();
	}
}
