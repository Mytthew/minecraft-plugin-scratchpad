package eu.mytthew.test;

import lombok.Value;
import org.bukkit.Material;

@Value
public class KitItem {
	Material material;
	int amount;
}
