package eu.mytthew.test;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class Config {
	RandomCollection<String> textCollection = new RandomCollection<>();
	List<KitItem> itemList = new ArrayList<>();
	Map<String, String> messagesMap = new HashMap<>();
	int duration;
}
